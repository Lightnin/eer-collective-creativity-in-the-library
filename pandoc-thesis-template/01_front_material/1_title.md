\newcommand\titleLineOne{Experimenting, Experiencing, Reflecting: Collective Creativity in the Library}
\newcommand\titleLineTwo{}
\newcommand\thesisAuthor{Amos Blanton, MA EdS}
\newcommand\supervisor{Dr. Andreas Roepstorff}
\newcommand\coSupervisor{Dr. Peter Dalsgaard}
\newcommand\degree{PhD in Arts}
\newcommand\thesisYear{2023}
\newcommand\revision{Rev. 1.01 15 August - Revised article "Experiments towards a Pedagogy of
Creativity and Learning in the Library" }
\newcommand\institution{Aarhus University}
\newcommand\address{Aarhus, Denmark}
\begin{titlepage}
    \begin{center}
    
        \vspace*{1cm}
        

       \large{ \textbf{ \uppercase{\titleLineOne 
       \\
        \titleLineTwo}}}
        
        \vspace{0.5cm}
        
        \vspace{1.5cm}
 
        Submitted in partial fulfilment for the degree of
        \\
        \large{\textsc{
        \degree
        }}\\
        \institution
        \\
        \address
        
        \vspace{0.8cm}        
         
        by\\
        \textbf{
        \thesisAuthor
        }
        
        supervisor\\
        \textbf{
        \supervisor
        }
        
        co-supervisor\\
        \textbf{
        \coSupervisor
        }
        
        \vfill
  
            \thesisYear
            \revision

     \end{center}
    \thispagestyle{empty}
\end{titlepage} 

\newpage
\thispagestyle{empty}
\mbox{}
