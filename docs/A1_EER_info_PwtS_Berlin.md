

# Appendix 1: Berlin Playing with the Sun EER Workshop

This documentation appears on the EER project Website along with a brief video documentation of the activity at this URL:  [https://www.eer.info/activities/playing-with-the-sun](https://www.eer.info/activities/playing-with-the-sun). It was written for a public audience as a brief informative piece about EER research. The essay and link to this seven minute video [https://player.vimeo.com/video/799029719](https://player.vimeo.com/video/799029719) are included here as part of the materials submitted for consideration by the PhD committee.

![Screen capture of EER Playing with the Sun page, containing documentation of the Berlin workshop at PSM art gallery, January 2023.](assets/EER-info-PwtS-page.png){width=66%}

## Playing with the Sun, EER.info website


As children create new tinkering projects, they construct an understanding that’s meaningful and relevant because it's driven by their own curiosity and creativity. <a href="https://www.playingwiththesun.org/">Playing with the Sun</a> is a collection of activities, play materials, and pedagogy designed to help learners build an intuitive sense of how sustainable energy works through playful, open-ended tinkering. This research explores how to create the conditions for people to ask and answer new design questions together as a collective.

The workshop described below was held at PSM gallery in Berlin in January of 2023, and was designed and led by Amos Blanton as part of his research into collective creativity.

![Two workshop participants at the EER PSM Gallery.](assets/girls-playing-hands.jpg)



### Design

![Video documentation of <a href="https://player.vimeo.com/video/799029719">Playing with the Sun EER workshop, Berlin January 2023</a>](assets/AB-Berlin-Vimeo.png)

How can we create the conditions for children to not only solve problems, but to invent new problems to solve? After their individual explorations into an open-ended design space, how can we help the group reflect on and choose which ideas to explore further? These skills are at the foundations of collective creativity, what Brian Eno referred to as "Scenius" or the collective form of genius. This work builds on the pedagogy of tinkering and constructionism to create conditions conducive to a short-term form of scenius. The goal is for children to build their knowledge of how sustainable energy works while working towards shared goals they themselves define.

### Run

In January of 2023, students from the International Kant School, Berlin, visited PSM Gallery to participate in this EER experiment. Earlier that morning, artificial for playful experimentation using sand and cinefoil. There was no sunshine in Berlin that day, so Amos setup a halogen worklight over one area of the table and provided <a href="https://docs.playingwiththesun.org/Hand-Crank-Generator/">hand-crank generators</a> and chargeable <a href="https://docs.playingwiththesun.org/Power-Pack/">powerpacks</a> as an alternative, human-powered power source. These are a few of the elements in the <a href="https://docs.playingwiththesun.org/Const-Kit-Overview/">Playing with the Sun Construction Kit,</a> an open source project to develop play materials for children to build with and learn about sustainable energy.

![Participants building at the start of the workshop.](assets/3boys-amos-pwts.jpg)


![Experimenting with the crank charger.](assets/girl-crank.jpg){width=66%}

After demonstrating how the different elements in the construction kit work, Amos invited the children to build a creature powered by sustainable energy that interacts with the sand-filled environment on the table. As they tried out different ideas, Ida [Last name?] documented them by recording videos with an iPad, printing out still images and placing them on the gallery wall. Using AR software called Artivive, the learners could see the videos of their projects in action at different stages by pointing the iPad's camera at the still image. The documentation wall served as a live-updating repository of the children's emergent ideas, which they could then reflect on as they explored. It was also a means of collecting data to create a map of their creative process.


![Assembling a solar creature with images of recent builds in the background.](assets/girl-build-bkground-rp.jpg)

Under the relatively weak artificial lighting, it was difficult to build a creature strong enough to pull the weight of its own solar panel unless it was directly under the lamps. One participant recognized this challenge and invented a solution. She placed the solar panel in the ideal location under the lamp and joined six wire segments together to make a long cable. Like a cable running between a solar farm and a city, she used it to transmit solar energy from where it was generated to her creature.

![Positioning the solar panel for optimum light.](assets/hands-solar-panel.jpg)

![The tethered creature powered by the solar panel on the mountain.](assets/two-wheels-long-tether-machine.jpg)

After the first 45 minutes of experimentation, Amos asked the participants to stop and discuss what emerged from their process that they felt was interesting and worth exploring further. The stationary solar panel with tether idea featured prominently. Two other ideas emerged: two-wheeled creatures, and drawing interesting patterns in the sand. After the discussion they spent another 40 minutes tinkering with these ideas as self-imposed design constraints intended to give focus to their collective inquiry.

![The participant's definition of quality.](assets/what-makes-cool-creature.jpg)

At the end of the workshop the participants had created a sub-genre of two-wheeled sand creatures. The students expressed appreciation and seemed to enjoy the workshop. When asked what they would tell a future group of participants exploring a similar activity, they said that they'd had fun exploring two-wheeled solar power tethered creatures, but felt they'd explored most of what was possible in that particular corner of the design space. 

### Learn

*How can we create the conditions for children to not only solve problems, but to invent new problems to solve?* 

In the classroom, a successful student gives the correct answers to questions asked by their teachers (who already know the correct answers). In art and design, the question is often invented at the same time that it's being answered. Out of this messy, iterative dialog, both new framings of problems and new creative solutions can emerge. 


![Describing exciting possibilities.](assets/boys-build-enthusiasm.jpg)


In the coming decades of the climate crisis, this kind of creativity will be  more important to our survival than recalling the right answers to questions that are already defined, which computers can already do far better than we can. Therefore the only correct answer in this activity is the answer to a question or problem that the children themselves have posed. One example from this workshop is the invention of the long cable that allows the creatures to be powered by a stationary solar panel. This is not an idea that was present in anything that was demonstrated to the children at the beginning. Nor was the problem that led to this particular solution framed for them in advance. This suggests that the structure built into the design of the environment, materials, and facilitation in this workshop was simultaneously *enough* and *not too much* to create space for their curiosity to lead. 

*After their initial individual explorations into an open-ended design space, how can we help the group reflect and choose which ideas to explore further?*

During the workshop, one participant noted that the staccato movement of his creature, caused by an energy storage component called a <a href="https://docs.playingwiththesun.org/Solar-Engine/">solarengine</a>, makes it appear as though it was a stop motion animation. Children tend to use descriptive metaphors like this as they reflect on their experience, both as a means of synthesizing knowledge and to communicate subtle ideas. Sometimes they lead to new question and problems to pose.

![Building together.](assets/girls-hands.jpg)


The strategies used to create the conditions for problem posing have been established for decades in progressive education (even if they are used too rarely in many schools). But how to create the conditions for collectively creative inquiry is less clear. There were two interventions designed to support collective creativity in this workshop. 

The first was the collection and posting of images and augmented reality video recordings of their past builds to be used as shared notes to facilitate group reflection. This is intended to function as a kind of designer's notebook for the collective that participants can refer to as they choose which new directions to explore. The second intervention was structuring the workshop with a beginning tinkering session, followed by a reflection session in which the children discuss and select future directions to explore, followed by a second round of building based on the constraints they agreed upon. 

While the pre-lunch break discussion about what they'd found interesting so far seemed natural, the post-lunch conversation felt a bit forced. And while they were impressed by the video collection, it didn't clearly lead to the building of insights or stronger reflection. Both ideas show some promise, but need further iteration and development by research practitioners and design researchers.

One of the aspirations of Playing with the Sun is to create the conditions for collective exploration at two scales: The workshop scale (described here) and the broader scale of the area of inquiry itself. Rather than seeing a workshop as a one-off educational intervention, we document it as participation in a collective design experiment spanning both time and participants. The next group that does this workshop should be able to use the map of its prototypes as the starting point for their exploration.

![The recursive prompting board on display in the gallery.](assets/rp-board-PSM.png){width=66%}


Design processes like these can and should be a means of democratic inquiry - an approach that could come in handy whenever local communities need to think together about how to respond to wicked problems like climate change. 


![Creative engagement.](assets/boy-fascinated.jpg)

### Acknowledgements

We would like to thank the students who participated from the International Kant School and their teachers, PSM Gallery, as well as Ida Thomsen for her work helping to document.  Thanks to Yanina Isla for the photographs and videography.

This workshop evolved from earlier work in EER by Amos Blanton on <a href="https://www.eer.info/activities/drawing-with-the-sun">Drawing with the Sun</a>. The activity itself was developed in collaboration with the technological literacy group at Aarhus Public Libraries. 

The <a href="https://www.playingwiththesun.org/#pedagogy">pedagogy of Playing with the Sun</a> is based on a learning theory called Tinkering (Wilkinson & Petrich, 2013) which is inspired by artistic processes and employed in science centers around the world. Amos' research into collective creativity is informed and inspired by the work of the children and educators of Reggio Emilia, Italy (Vecchi, 2010). All of these theories aim to create the conditions for what Eleanor Duckworth (1972) called "the having of wonderful ideas," and form the foundation for this research.

### References

Duckworth, E. (1972). The Having of Wonderful Ideas. Harvard Educational Review, 42(2), 217–231. [https://doi.org/10.17763/haer.42.2.g71724846u525up3](https://doi.org/10.17763/haer.42.2.g71724846u525up3)

Vecchi, V. (2010). Art and Creativity in Reggio Emilia. Routledge. [https://doi.org/10.4324/9780203854679](https://doi.org/10.4324/9780203854679)

Wilkinson, K., & Petrich, M. (2013). The art of tinkering: Meet 150+ makers working at the intersection of art, science & technology. Weldon Owen.
