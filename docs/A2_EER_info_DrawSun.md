



# Appendix 2: Copenhagen Drawing with the Sun EER Workshop

This documentation of an early Playing with the Sun workshop appears on the EER project Website at this URL:  [https://www.eer.info/activities/drawing-with-the-sun](https://www.eer.info/activities/drawing-with-the-sun). It was written for a public audience as a brief informative piece about EER research that took place in September of 2021. It is included here as part of the materials submitted for consideration by the PhD committee.

## Drawing with the Sun, EER.info website

How do ideas move through a group of people exploring an open-ended activity, and do they inspire new ideas in the process?

Drawing with the Sun is a 90 minute workshop designed to explore how groups engage in collective creativity in tinkering activities. Tinkering is an open-ended, exploratory approach to learning inspired by the work of Seymour Papert. In this workshop we invited participants to modify solar powered drawing machines to make marks they felt were aesthetically interesting. A smaller portion of the participants were asked to document and spread new ideas as they emerged.

![Workshop participants shining additional light onto solar drawing machines.](assets/mirrors-machines.jpg)

### Design

Solar drawing machines was developed as part of a larger initiative called [“Playing with the Sun”](https://www.playingwiththesun.org/) - a practitioner-researcher collective that creates activities that  invite people to play with the elements of sustainable energy -  envisioned by Amos Blanton, Ben Mardell and Dokk1 Library in Aarhus,  Denmark.

Playing with the Sun invites educators to take on the  role of facilitators of design explorations led by the participants.  Using provocations like solar drawing machines, we invite the learners  to playfully imagine and create whatever they like. As they explore, we  document the process so their insights accrete and can form a new  starting point for future participants. Our goal as designers is to  create play materials that invite children and adults to develop an  intuitive understanding of the elements of sustainable energy. And as  researchers, we try to document and understand how this shared learning  process works, and how we can design environments and prompts to support it. As Seymour Papert put it, a toy can be "an object to think with"  which reveals new possibilities for understanding and interacting with  the world.

![Hands holding a drawing machine.](assets/hand-holding-machine.jpg){width=66%}

![Participants around the shared drawing table.](assets/people-mirrors-table.jpg){width=66%}



![Video of the interaction around the shared drawing table, visible at: <a href="https://www.eer.info/activities/drawing-with-the-sun">https://www.eer.info/activities/drawing-with-the-sun</a>](assets/pwts-shared-draw-table-CPH.png)



### Run

**How do ideas move through a group of people exploring an open-ended activity,  and do they inspire new ideas in the process?**

In this workshop we invited 14 participants to tinker with solar powered  drawing machines while 6 "Catalysts" documented the emergence and  movement of new ideas through the collective. The goal was to understand and begin to map the process of collective creativity - or how a group  playfully imagines and explores a space of possibilities.

Each  pair of tinkerers was provided with a working drawing machine,  consisting of a solar panel, a motor, and a simple solar engine (a  circuit designed to store solar energy in a capacitor until just enough  is present to pulse the motor for 1/4 second. The machines do not have  batteries.) We setup worktables covered with drawing paper outside, and  made available a variety of pens, paints, and markers, along with clamps and other materials to enable participants to make a wide variety of  creative modifications to their machines. Each worktable also held  several A4 sized mirrors and stands, which the tinkerers soon realized  could be use to reflect additional sunlight onto their drawing machines, which increased their speed and power.

During the workshop they  were lightly facilitated by Liam Nilsen and Amos Blanton, who provided  technical support, advice, and feedback when needed.

![Milanote board of documentation of the workshop. An interactive version is visible on the Milanote website at <a href="https://app.milanote.com/1N5Oxi1L7tMr8Y?p=7qEZrvV8sun">https://app.milanote.com/1N5Oxi1L7tMr8Y?p=7qEZrvV8sun</a>.](assets/milanote-collective-tinkering.png)

### Learn

In the above Milanote board there is a map showing the design process of  three pairs of participants, highlighting points where ideas moved  between the groups due to the actions of catalysts. In the bottom row,  Karsten and Julia discover that their drawing machine seems weaker than  most, and ask for and are given a replacement. But having formed a sentimental attachment to their first machine, they decide to join the new one to the original in order to help it, thus inventing the first  combined solar drawing machine. This idea of combining machines is then shared by their catalyst with two other groups present at the shared drawing table, a space which is  designed to invite encounters between groups to facilitate  cross-pollination of ideas and collective reflection.  One of these two groups named their machine "Bonkers," inspired by its erratic movements  when given extra sunlight via mirroring. The two groups decide to try  combining their two drawing machines together, and create  "Frankenbonkers." In the process, they have a  conversation about collaboration and togetherness. 

Out of this conversation (shown in the slightly larger video positioned  between the first and second rows) several new ideas emerge. One worth noting is the idea of attaching mirrors to the machines themselves, so  that they can shine light onto one another. This suggests a new domain of interactivity (communication?) between machines. I argue that the idea is an emergent property of the group's playful inquiry - more a  product of "Scenius," (Brian Eno's term for the collective form of  genius) than "Genius."

**The Adjacent Possible**

Proposed by the biologist Stuart Kauffman, the adjacent possible is a useful concept for understanding the exploration of a space of possibilities  in a collective tinkering session like this one. Simply put, the  adjacent possible is what’s next door to whatever state something is in  right now. Before the Post-It note existed, it was an adjacent possible  of the plain paper note taped to a wall. Once invented, it became a new  “actual” from which new adjacent possibles could emerge in various  realms, from using them to make fish scales in craft activities to a  tool for organizing collections of thoughts in design meetings.  Kauffman's work suggests that even the most dramatic changes in both  design processes and the evolutionary fossil record can be explained by a series of small steps between adjacent possibles.

In the documentation shown above, we can see evidence of movement between  adjacent possibles that results in a new idea, an idea that no one in  the group imagined prior to the collective tinkering activity. First the idea of combining two machines is born. It is then reinterpreted by a  different group, which appears to lay the foundation for an innovation  to emerge out of the participant's interests exploration of  interdependence - the idea of drawing machines that reflect light onto  (and thus interact with) one another. While this is just one of many  ideas that emerged in the workshop, we emphasize it here because it  opens up a new design realm that didn't exist before -- that of  interactivity between solar drawing machines. That realm, while  admittedly complex and challenging, has interesting potential for  further creative exploration.

It is always difficult to draw firm  conclusions about the origin of ideas. Because tinkering is open-ended  and sensitive to what the participants bring to it, any tinkering workshop is at least as complex as Heraclitus' famous river. You cannot  step into the same tinkering workshop twice. But we do not need to make  claims of strict repeatability to document, observe, and theorize about  how best to support collective creativity.
