

# Appendix 3: Learning about Facilitating Creativity from Parents and Grandmothers

This documentation appears on the Aarhus Public Libraries Website at this URL:  [https://www.aakb.dk/nyheder/kort-nyt/learning-about-facilitating-creativity-from-parents-and-grandmothers](https://www.aakb.dk/nyheder/kort-nyt/learning-about-facilitating-creativity-from-parents-and-grandmothers) . It was written for a public audience as a brief informative piece about research done by the Creative Learning Research group in Aarhus Bibliotekerne. Extensive editing and input was given by the members of the Creative Learning Research Group. The piece is included here as part of the materials submitted for consideration by the PhD committee.

![Screenshot of this essay on the Aarhus Public Libraries website.](assets/learn-facil-biblio-screen.png)

## Learning about Facilitating Creativity..., Aarhus Bibliotekerne Website

![Helping people learn to trust their own process of creative learning involves being available to give help and support without getting in the way.](assets/pigemedsav.jpg)

Recently the [Creative Learning Research Group](http://Recently the Creative Learning Research Group at Dokk1 hosted a workshop developed by En Hemmelig Klub called Papalapap.) at Dokk1 hosted a workshop developed by En Hemmelig Klub called Papalapap. In Papalapap, citizens of all ages are invited to build with  cardboard. In their excellent workshop design, [En Hemmelig Klub](https://schhh.net/) provided a thoughtful constraint in the form of starting points for people to begin their tinkering. They were invited either to build a link in a chain of cardboard marble runs that stack on top of one another, or to build a tetris piece that can interlock with other similar pieces.

Over the past year in the Creative Learning Research group at Dokk1 we have spoken often about the right amount of constraints in open-ended activity design. As long time facilitators of creative activities, each of us has experienced inviting a learner to “make whatever you want,”  with little to no constraints. This generally seems to work only for those who are already very confident in their creative abilities, and  know what they want to do. But the rest often don’t know where to begin, and get stuck at staring at a blank piece of paper. The art of facilitating a good tinkering activity is to find a constraint that is narrow enough to help the learner get started, but open enough that they can make something unique and meaningful that reflects their own  interests and curiosity. 



​    ![View of the Papalapap workshop at Dokk1 Library in Aarhus.](assets/rampen-20230601133747493.jpg)

Our position is that everyone is creative, but some have more trust in the creative process than others. During this workshop we were watching for patterns in the way participants exercised their creativity and self-expression in order to help develop our understanding of how best to facilitate creative experiences. While reflecting on our  observations afterwards, we noticed that the parents and grandparents demonstrated several different ways of supporting children’s creativity that we felt we could learn from. These descriptions consist of our own interpretations - we make no claims to objective truth. Nonetheless, we feel that what we describe here can be useful for our ongoing  conversations about how to design and facilitate creative activities. 

During the workshop we observed one girl become excited about making a cardboard dragon. While technically this falls outside of the constraints we suggested at the start, it was a kind of modification or  “hack” of our proposal - one which the child was clearly both capable  and motivated to explore.  Her parents, who seemed like they had experience with the creative process, provided support and help - but  not so much support and help that they took over.  At one point they suggested the child sketch out a few post-it-notes of different possible leg designs to help imagine and choose what would work best. Out of  this creative process a beautiful dragon emerged.

​    ![The dragon emerges through an iterative process, including sketches on post-it notes.)](assets/billede3_4.jpg)

A grandmother we observed used a different approach to helping  her grandson follow through on his creative process. When the grandson lost interest in their common project, she would continue the work by  herself. After a little while, she would invite him to join back in by  posing interesting problems or design decisions to him. “How will we get the marble around this corner?” she might ask, or “What should this part look like?” After several prompts like this, the grandson would join back in and become engaged with their project again. Eventually, they ended up completing an addition to the marble run chain that  randomized the direction the marble would go - an idea that was both  challenging and innovative.

Not everyone has had the chance to develop trust in their creative process. We sometimes encounter adults who, when invited to be playful  and creative, throw up their hands and say “I’m not a creative person!”  When that happens we’ve learned to intervene in different ways, with the goal of helping them find a sense of “flow” so that they forget to be worried about being creative. Like helping a child learn to ride a bike, the trick is figuring out the best way of supporting them as they get  the hang of it. Much as we might want to, we cannot learn to ride the bike for them.

Out of this workshop, and the discussions we had about it afterwards  in the Creative Learning Research Group emerged a new question. How do we communicate the idea of trusting in the creative process so that  everyone, even those who don’t yet think of themselves as creative, can benefit from it?  What sort of statement or set of guidelines can we highlight in our various workshops to help people feel more comfortable being playful and creative? It’s difficult to sum up an approach to  creativity in a few words. But there’s lots we can learn from watching  parents and grandparents as they work with children. So we’ll be observing and thinking about this question in the months and years to come.

