# Article: A Short-term Ecology for the Having of Wonderful Ideas: Collective Creativity and Cross-Pollination

Amos Blanton, MA EdS.
Draft 7 June 2023.
Under review as a chapter for a forthcoming EER Book Project.

A good idea has emerged. Five people are standing in the sun around a table on which two solar powered drawing machines are spinning. They've spent the last 40 minutes tinkering together in pairs, chatting and playfully riffing on different designs with one another. At the start of the workshop they were given acrylic mirrors which they use to reflect additional sunlight onto their machines, which makes them go faster. One pair has a small section of mirror, about the size of an index card, taped onto their machine. Instead of making it go faster, their idea is to have their machine reflect sunlight onto the other machines around it. When asked why they've done this, one of them explains with an African proverb. 

"If you want to go fast, go alone. If you want to go far, go together." 

![The drawing machine with mirror attached, visible at low center.](assets/EER_Runa_Huber_MG_1263.jpeg){width=66%}

This prototype with its attached mirror has opened up the possibility of communication between machines, a domain of potential that did not exist before this moment. That the machines might become interactive, reflecting sunlight onto and influencing one another, is an idea that has never occurred to the designer of the activity. It suggests a whole new realm of possibilities.

It's a good idea. Where did it come from? 

The conventional answer to the question of who "had" the idea doesn't make for much of a mystery, and anyway its on video. It was Joe. Acting in his assigned role as *catalyst*, Joe, an anthropologist in his fifties, was riffing on ideas with six tinkerers at the shared drawing table. 

At the start of the activity, six of the twenty participants, a collection of artists and academics from the Experimenting, Experiencing, Reflecting project (EER) (http://EER.info), were assigned the role of *catalyst*. Catalysts were given a phone with a video camera and asked to attach themselves to one of the seven pairs of tinkerers building drawing machines together. They were told to record a brief video of any idea that arose and ask their tinkerers to say a few words about it. If they felt the idea was a good one, they could show it to another catalyst and, if both agreed the timing was right, play it for the second catalyst's pair of tinkerers as inspiration. In addition to these instructions, the catalysts were invited to do whatever they thought might be conducive to supporting and documenting cross-pollination and the emergence of new ideas.

The video shows a brief pause in the interaction at the shared drawing table. And Joe says:

> "I just had this vision of like putting the mirror on one of them and so as it moves around then it shines on another one who moves around who shines on..."

Joe had the idea.

When we use the English construction "Joe had the idea," we are referring to an idea in the same way we refer to a physical object. We use "Joe had the pen" or "Joe had some nachos," to describe possession of something. [^joehadbaby] But an idea is not the same as an object, even if intellectual property law says that an idea can be owned. 

The idea that a good idea can be "had" by someone is important in Western culture. It's supposed to be both an engine of social mobility and a justification for wealth disparity. We grant immense wealth and power to people who have good ideas. But where exactly are the borders of an idea that allow it to be had by some *one*? Where is an idea's skin, the place where the idea ends or begins depending on if we are moving into or out of it? Did the idea of attaching mirrors to the drawing machines leap fully formed out of Joe, like Athena leapt full-grown out of Zeus' forehead? Or is it more realistic to say that the existence of a good idea is proof of some past relationship, the same way the existence of a baby is?

To answer this question in this experiment we can begin by examining the ancestry of Joe's idea in the evidence collected during the first 40 minutes of the activity. If there is more than one antecedent (and there is), who came up with those ideas, and how did they get into Joe? The conventional whodunnit mystery of who "had" the idea is already solved. But this was a stakeout to get to the bottom of a bigger question, one which we must borrow the grammar of the American South to ask: Who-all dunnit?  

The musician and producer Brian Eno has stated that many good ideas are the product of what he calls "Scenius" — the collective form of "Genius" used to describe the creative intelligence of a music or art scene (Frere-Jones, A. 2014). If this is true, how can we create the conditions for studying how the collective form of genius works? And what methods can we use to analyze it? If the research pointing to the importance of collective creativity is correct, then it is a powerful means of creating conditions that are conducive to creativity and innovation (Hippel, 2005; Sawyer 2014). We are entering a phase of human history in which we're going to need more of that. 

## Design

Collective creativity is the emergence of innovative ideas from a group of individuals working with a shared purpose. Different disciplines have attempted to grapple with the complexity of collective creativity in different ways. Business research tries to describe the key practices of organizations that cultivate it (Catmull, E. 2008; Parjanen, S. 2012). Von Hippel (2005) studied how it emerges in sub-communities of enthusiasts that drive innovation in various domains. And in psychology, Sawyer (2014) described the interactions between individuals and the group that lie at the foundation of collaborative creativity in improvisational theater and music. Each of these researchers gather data on collective creativity in the ethnographic "wild," which they use to describe the qualities of a process that is as complex as it is sensitive to subtleties of context. 

While the ethnographic literature contains many trenchant observations about collective creativity, it also shows an area of untapped potential. Most of it comes out of relatively long-term study of communities of skilled enthusiasts. The approach used here is inspired by the Experimenting, Experiencing, Reflecting project (http://EER.info ), and seeks to discover what can be learned by turning each of these factors on its head: What can we find out by inviting un-skilled enthusiasts to be creative together in a short-term activity? Rather than design a laboratory experiment that attempts to quantify effects by excluding confounding variables, this method proposes that we embrace contextual richness in all its complexity, and observe what happens as the process unfolds. 

The goal of this case study was to create the conditions for collective creativity and to explore strategies for documenting and analyzing the interplay between individuals and the group. Creativity is defined as novel actions or ideas, particularly those that emerge from recombination in different ways or applications to new situations (Bateson & Martin 2013). The first research question is aimed at a foundational aspect of collective creativity: the cross-pollination of ideas.

- (RQ 1) How can we catalyze the cross-pollination of ideas through group reflection in a tinkering activity, and is there evidence that this leads to the emergence of new ideas through collective creativity?

The goal of this experiment was to boost cross-pollination via two interventions: 1) putting people in the role of catalysts, already described above, and 2) by creating a shared space for collective reflection at the shared drawing table, which will be described below. The emergence and movement of ideas was captured by the catalysts and an additional camera pointed at the shared drawing table.

Once the experiment was complete a second question soon emerged. The original goal was to create artificial conditions for an extremely brief and unusually dense ethnography — a "reduction" more in the spirit of cuisine than psychology. But the analysis soon came up against the limitations of the medium. Writing requires serialization that lends itself to narrative. But to see where the narrative of a single idea emerges requires a record of the movements of many different ideas through the group. This led to the second research question, which pertains to the analysis and presentation of the data.

- (RQ 2) How can we represent the collected data in a way that shows the interaction between participants in order to foreground collective creativity as an object of study? 

Such a record of data collected should provide more than just the evidence used in the ethnographic narrative that (eventually) emerges. If we are making up stories to try to explain observations (as, arguably, all researchers of complex human phenomena are), it behooves us to provide more than just the data that supports our own story. It should be possible for others to view the same data and provide alternative interpretations. This data should be not only available, but accessible in such a way that requires little to no technical expertise to understand. 

## Run

![Karsten and Julie's idea to attach two drawing machines together emerges in the timeline.](assets/karsten-julie-2.png){width=66%}

> *Karsten: "This particular guy or girl is not strong enough in other positions. So this is the position where it can flourish."*

Karsten and Julie have been working on their drawing machine for about ten minutes. Their impression is that it's weaker than the other drawing machines they see around them. They try re-positioning the motor and drive-wheel in different ways, but it remains sluggish until they move the wheel to a perpendicular position, parallel to the solar panel. With the wheel acting as a base around which the machine rotates, and a soft brush pen in place of a stiff marker, it begins drawing big orange circles. 

They mention the weakness of their machine to one of the facilitators, and he gives them a spare device held in reserve as a replacement. Instead of discarding their original machine, they begin attaching the two machines together to make one large drawing machine. Peter, their catalyst, starts filming, and asks how this idea came about. 

> *Karsten: "The idea was that we first had a weak drawing [machine], and then we got a stronger one. And then we liked the weaker one better. So now we wanted to see if maybe they are just better together."* 

A little later, a catalyst from a different group comes by to offer them the idea of naming one's machine. He explains how his pair of tinkerers named their machine "Bonkers," because they liked to make it "freak out" and "go bonkers" by reflecting extra sunlight onto it with mirrors. Peter, their catalyst, asks for their reaction to the idea of naming, and to the name "Bonkers." 

> *Karsten: "I thought it was the opposite of our relationship because we've been very caring... Making it freak out is the furthest away from my mindset right now..."* 
>
> *Peter: "So you are susceptible to the idea of naming, but your relationship is different? More about maybe caring or nurturing?"*
>
> *Julie: "We are definitely not that inspired by 'Bonkers,'...we wouldn't choose that."*

Peter takes the video of Karsten and Julie's two drawing machines grafted together over to the shared drawing table, where he presents the idea of combining two machines together to three pairs of tinkerers working there. One of the catalysts there is called Joe.

### Cross-Rejection of Ideas

When we think of cross-pollination, we might imagine an idea moving from one group to another, like a beautiful dandelion seeds floating across the landscape. It is welcomed in its new home as something foreign and exciting, something that inspires new possibilities, and new ways of thinking. 

This moment with Karsten and Julie also involves the introduction of new and foreign ideas, but with a different result. To them, this new idea is notable not for its exciting new potential, but for its contrast to their way of thinking. Later on, Karsten and Julie describe their relationship to their machine as "curling parents"[^curling] — a pejorative Danish idiom similar to the American "helicopter parents," used to describe parents who are overly involved and attentive to their children. To them, the idea that someone named their drawing machine "Bonkers" is distasteful, like hearing that someone named their child "Bonkers," and liked feeding them sweets to make them "freak out."  Watching the video, one gets the sense that by rejecting the cross-pollinated idea, they are both articulating and affirming their own position, playful as it is. 

### Theory for Creating a Short-Term Ecology of Wonderful Ideas

"The role of the teacher is to create the conditions for invention, rather than provide ready-made knowledge."  - Seymour Papert

Eleanor Duckworth, professor at Harvard School of Education and author of the seminal essay *The Having of Wonderful Ideas*, framed her view of learning this way. 

> "The having of wonderful ideas is what I consider to be the essence of intellectual development. And I consider it the essence of pedagogy to give [the child] the occasion to have his wonderful ideas, and to let him feel good about himself for having them" (1972).  

Duckworth's *wonderful ideas* are not confined only to creative or novel insights. The child may have wonderful ideas in the form of concepts that have long been understood by science. But the phrasing "having of wonderful ideas" reflects a fundamental tenet of constructivist learning theory: that the child builds their own understanding through a process of observing, experimenting, reflecting, inventing and refining their ideas. The teacher's goal is therefore "to give [the child] the occasion to have his wonderful ideas." 

Building on the work of Duckworth and Seymour Papert's Constructionism (1982), the pedagogy of Tinkering is a means of creating the conditions for invention in non-formal learning environments like science museums, makerspaces, and libraries (Vossoughi & Bevan, 2014). Inspired by artistic processes (Wilkinson & Petrich, 2013), Tinkering educators specialize in the design and facilitation of short-term, open-ended learning experiences that enable the creative exploration of various scientific and aesthetic phenomena. 

The word *tinkering* itself refers to creative exploration that is open to emergent goals and encounters with new ideas.

> Sometimes, tinkerers start without a goal. Instead of the top-down approach of traditional planning, tinkerers use a bottom-up approach. They begin by messing around with materials (e.g., snapping LEGO bricks together in different patterns), and a goal emerges from their playful explorations (e.g., deciding to build a fantasy castle). Other times, tinkerers have a general goal, but they are not quite sure how to get there. They might start with a tentative plan, but they continually adapt and renegotiate their plans based on their interactions with the materials and people they are working with. (Resnick & Rosenbaum, 2013)

While there are elements of design for cross-pollination in the (mostly oral) Tinkering and constructionist tradition (Resnick & Rusk, 1996), most of the focus is on supporting individuals leading their own learning experiences. The educational approach developed by the children and educators of Reggio Emilia, Italy, while sharing many of the same fundamental values and thinking around learning, tends to think more about the collective (Rinaldi, 2006). In discussions with teachers and pedagogistas adept in the Reggio approach, one will often hear the 'class' (meaning everyone in the classroom) discussed as though it were an entity, one with its own unique interests, curiosities, and foibles. The class-as-entity is in dialog not only with the teacher (who is a leader as well as a member), but also with each student as an individual.

Tinkering, Resnick's creative learning (2017), and the Reggio Emilia Approach are all approaches to learning that are driven by practitioner researchers, but the tradition from Reggio Emilia is the only one to have created and exported its own research methodology. What is referred to as "Documentation" (Giudici et al., 2008) in Reggio Emilia is defined as "The practice of observing, recording, interpreting and sharing through a variety of media the processes and products of learning in order to deepen and extend learning" (Krechevsky et al., 2013). Collecting Documentation puts educators in a role similar to that of ethnographic researchers, gathering evidence of how the children use different activities to develop and refine their knowledge and creativity. Documentation is the evidence that grounds conversations between educators in Reggio Emilia as they reflect on what is happening in the classroom, and informs their subsequent interventions with the children. Though the means of Documenting collective creativity developed and used in this research would be considered far outside the orthodoxy, they do take their inspiration from Reggio.  

The design of the solar drawing machines activity itself relies on a foundation in Tinkering, learned in practice from other non-formal educators via an oral tradition. The prompt given to the learners at the beginning was: "Begin by seeing what your machine draws. Then change it to make it make drawings that you find interesting." For those who may feel nervous or don't know how to begin, this prompt gives a clear starting point, something to do and to observe: "Begin by seeing what your machine draws." The next sentence invites the learner to follow their own agency and choice. Good prompts tend to follow this pattern of first grounding the learner and then inviting them to explore something interesting.

Aside from the prompt, there are many other factors that are important to running a successful Tinkering activity.

- the tone of the introduction 

- the choices about materials to work with

- the design and "tinkerability" of the drawing machine "Base models" given as starting points

- the character and goals of the facilitation strategy

-  the setup of the surrounding environment

These and other elements too numerous to mention have been honed over thousands of workshops by Tinkering practitioners, and passed on as part of a (mostly) oral tradition. This is not a casual undertaking. Some educators have spent most of their working lives honing the craft of Tinkering design and facilitation. There is far more to it than can be described here and perhaps, since it is mostly an oral tradition, more than can be contained in the medium of writing. But there are practitioners experienced in running interest-driven creative activities like these in science museums, makerspaces, and schools all over the world. All that is required for doing more of this kind of research is finding one to collaborate with.



## Learn

![The interaction at the shared drawing table after Peter introduces the idea of joining two drawing machines together.](assets/shared-drawing-table.png){width=66%}

Peter introduces the idea of combining drawing machines together to the group standing around the shared drawing table. A few minutes after the tinkerers begin, the facilitator makes a brief interruption to introduce the shared drawing table. 

> "When you have your machine making a drawing that you like, bring it to the shared drawing table and let it add your contribution to our shared drawing." 

One of the aims is to have everyone's drawing machine contribute to a shared drawing, representing the creativity of the entire group. Another is to create a space where tinkerers can encounter and take inspiration from one another's creations. In the Tinkering tradition this is sometimes called a "watering hole," and it functions as a space for cross-pollination of ideas in a large group of tinkerers.

The three pairs of tinkerers at the shared drawing table find Peter's description of Karsten and Julie's "togetherness" interesting, but get distracted for a moment. Two of them discuss if they should join their models together or keep exploring their projects separately, and the consensus for the moment is to keep working separately. But the theme of togetherness continues and takes different forms in the ensuing conversation. 

> *Liz: "....my characters they start out very alone, but then open.. open to teamwork."* 
>
> *Amy: "...is there a way that we can work together by shining on rhythmically, like on the count?  Like if we all shine on and off?"*
>
> *Joe: "Sounds like a song: If we all shine off... If we all shine..."*
>
> *Liz: ".... would it make sense to like fit them together, like I wonder if Bonkers would.. "*
>
> *<slight pause>*
>
> *Joe: "I just had this vision of like putting the mirror on one of them and so as it moves around then it shines on another one who moves around who shines on..."*
>
> *Several, maybe all: "Oohhh.. that's coool!"* 
>
> *Liz: "That's a good idea!"*
>
> *Amy: "Maybe we should be in the circle, and they should be holding mirrors."*

Helene, who hasn't said much previously, asks "Do we have smaller mirrors?", and then goes off camera, apparently to find the necessary tools to cut out a small piece of acrylic mirror. Moments later she returns, and she and Neema begin working out how to attach it to one of the drawing machines. Meanwhile, Liz and anna have joined two machines together to form "Franken-Bonkers," the second combined drawing machine of the day.

### Mapping Collective Creativity

Sawyer & DeZutter (2009) analyzed transcripts of conversations in improv groups in order to unpack the process of collective creativity. But tinkering is a different kind of improvisation, and presents a different kind of problem in terms of how to go about studying it. The group as a whole is never all together working on the same thing at the same time, as they often are in improv theater or music settings. Instead they operate in small pairs, each one a subgroup of the whole. Most of the ideas were generated in those pairs and then transmitted to other pairs, either by the catalysts, encounters at the shared drawing table, or simply as a result of working in the same general vicinity. A methodological problem soon emerges in that the data needs to be rendered tractable at two levels: the "fine scale" level of the filmed interactions, and the "gross scale" level of the workshop as a whole. Only at this gross scale can we see the movement of ideas between pairs of participants. This problem led to the second, more methodological, research question.

- (RQ 2) How can we represent the collected data in a way that shows the interaction between participants to foreground collective creativity as an object of study? 

This study uses Milanote, a software designed to support artists and other creatives in visually managing their ideas, to make a gross scale map / timeline hybrid of the workshop that situates the fine scale video data. By creating timelines for each pair of participants showing notes and video of their experiments and situating them within a two dimensional map, we can look for evidence of ideas moving between the sub-groups. 

One of the ways maps convey information differently than writing is that they present information in a searchable two dimensional space rather than in a serialized "start to finish" form. Whereas the writer of articles hopes that the reader will read through all or most of her words, the maker of maps accepts that the reader will use only a fraction of the information the map offers -- whatever it takes to get from point A to B. The rest is not relevant. As a means of providing a lot of information without overwhelming the reader, the method of constructing timeline maps makes it possible to highlight the emergence of an interesting idea, and work backwards to find its antecedents. This is a bit like starting from the mouth of a river and traveling upstream to map the tributaries that feed into it. 

Lots of interesting and novel ideas emerged out of this workshop, as is reliably the case with any Tinkering workshop that is reasonably well designed and executed. The problem of how to render the glut of data collected is solved by choosing one idea and generating a map that shows how it came to be.

![The milanote board containing data from the Solar Drawing Machines workshop, September 2021.](assets/cross-pol-milanote.png){width=66%}

This map consists of embedded, playable video and field notes collected by catalysts, each laid out according to time on the X axis. There are three timelines from three separate pairs of participants shown (out of 7 total pairs tinkering that day). When viewed on the Milanote page, the videos can be seen and heard at the discretion of the viewer, providing a window into what was occurring at that moment as well as the tonal qualities of the interaction. 

To tell the story of the emergence of the idea of attaching mirrors to the machine, I made small screen captures of the part of the map corresponding to the transcripts and events described above. These two sections are outlined as white rectangles in the overall map shown above. The viewer is free to browse all of the collected data from the participants involved on the map, even that which is not directly relevant to the narrative, in order to make their own interpretation or to criticize mine.[^corpus] This ease of accessibility is an important aspect of Documentation for practice-based research. It is done in the belief that the conclusions and explanations will generally benefit from more practitioners viewing and sharing their own interpretations of the data. [^hiding] 

A live version of the Map can be found here: [https://app.milanote.com/1N5Oxi1L7tMr8Y?p=7qEZrvV8sun](https://app.milanote.com/1N5Oxi1L7tMr8Y?p=7qEZrvV8sun)

### From Who-dunnit to Who-all dunnit

From the data collected, we can see how our view of who "had" the idea of attaching mirrors to the machines is dependent on how wide of a lens we use to view the interaction. If we set a narrow focus, as we did in the beginning, it is Joe's idea. But the stakeout on collective creativity reveals that there is more to the picture.

The idea of joining two machines together, as well as the conceptual theme of *togetherness*, was introduced to the tinkerers at the shared drawing table by Peter, who brought it from Karsten and Julie. Karsten and Julie had no direct interaction with the other players. Nevertheless, the theme of togetherness that emerged out of their work clearly influenced the conversation that led to the good idea. 

The idea of attaching mirrors to solar drawing machines is itself part of an ongoing, rapid fire conversation — one which we might best describe as "riffing" on the theme of togetherness, in the same way musicians might riff together on a musical theme.  It comes hot on the heels of another idea, and is quickly followed by a third. It is Joe's voice that says it out loud. But would our idea have been remembered if Helene had not gone off to cut out the piece of mirror, so that she and Neema could tape it onto a drawing machine? This is important, because it changes the idea from just another utterance into a physical prototype, which then becomes an anchor for further conversation about the ideas.  

As Lim et. al. (2008) describe it, "Prototypes are used as a means to frame, refine, and discover possibilities in a design space." Just as Karsten and Julie's combined drawing machine becomes both an anchor and a vehicle for the idea of "togetherness," the prototype mirrored drawing machine becomes the physical avatar of a big idea: that the machines might interact with and influence one another through the reflection of light. 

Prototypes often serve as what Papert called an "object to think with" (1982), which Yasmin Kafai described this way:

> According to Papert, physical objects play a central role in this knowledge construction process. He coined the term “objects-to-think-with” as an illustration of how objects in the physical and digital world (such as programs, robots, and games) can become objects in the mind that help to construct, examine, and revise connections between old and new knowledge. (Kafai, 2005)

Papert was influenced by the work of the famous anthropologist Claude Levi-Strauss. In his seminal book *The Savage Mind* (2000), Levi-Strauss contrasts the analytical, abstract approach of Western Science with what he called bricolage, a "science of the concrete" used in primitive societies. Bricolage involves the recombination of existing materials to create new form and function, and is very similar to what Kauffman describes as evolution through exploration of adjacent possibles, evident in the fossil record (2014). Along with Latour (1999), Papert argued that many elements of the kind of thinking that Levi-Strauss characterized as concrete and primitive remain in and are important to Western science. 

> "The bricoleur scientist does not move abstractly and hierarchically from axiom to theorem to corollary. Bricoleurs construct theories by arranging and rearranging, by negotiating and renegotiating with a set of well-known materials." (Turkle & Papert, 1991) 

If bricolage and 'negotiation and re-negotiation with a set of materials' seems familiar, it's because it is precisely what this experiment was designed to do. The same can be said of all Tinkering activities, and the reason for this has to do with the way Papert though about learning. Objects-to-think-with represent a bridge between the concrete and the abstract. This bridge makes possible an ongoing, iterative conversation between the concrete and the abstract within the mind of the learner. It is through this conversation that the learner constructs their own hand-made, artisanal generalizations that they subsequently use to understand how the world works and how to interact with it.  

It may appear as though we have now started speaking about two different things: learning and creativity. But constructivists and constructionists see these as manifestations of the same underlying process operating in the learner. The *Having of Wonderful Ideas* (Duckworth, 1972) is as much about learning as it is about creativity. Resnick's rebranding of constructionism as a more generalized approach he calls "creative learning" (2017) is an intentional blurring of a boundary he sees as artificial and outmoded. It's all knowledge construction. Creativity is just the word we use to refer to the stuff that happens to be novel or fresh in one dimension or another.

According to Constructionists, objects-to-think-with play an important role in knowledge creation for individuals. Our evidence suggests they may have a similarly important role in collective creativity. Let's try framing the interaction as a conversation between the concrete and the abstract. 

Abstract: Two people form a kind of **affection** for their machine. 

Concrete: A prototype of *two drawing machines joined together*. 

Abstract: The catalyst shows a video of the prototype to another group, describing it as kind of **togetherness.** 

Concrete: A prototype of a *mirrored drawing machine*.

Abstract: A new domain involving **communication** between drawing machines emerges.

And this takes us to our African proverb. 

"If you want to go fast, go alone. If you want to go far, go together." 

This is a further articulation of the idea of togetherness that describes still another way of thinking about the interaction of the drawing machines, one that stuck firmly in the author's mind. 

### The Tralfamadorian Sex Hypothesis of Collective Creativity

The way the word "conception" is used to describe the origin of both people and ideas is reminiscent of a passage from a famous Kurt Vonnegut novel called *Slaughterhouse 5*. Billy, the hero of the book, is living among the Tralfamadorians, a race of inter-dimensional aliens who experience reality in four dimensions, giving them complete access to past, present, and future all at once. 

> One of the biggest moral bombshells handed to Billy by the Tralfamadorians, incidentally, had to do with sex on Earth.  They said their flying-saucer crews had identified no fewer than seven sexes on Earth, each essential to reproduction.  Again: Billy couldn't possibly imagine what five of those seven sexes had to do with the making of a baby, since they were sexually active only in the fourth dimension.
>
> The Tralfamadorians tried to give Billy clues that would help him imagine sex in the invisible dimension. They told him that there could be no Earthling babies without male homosexuals.  There could be babies without female homosexuals.  There couldn't be babies without women over sixty-five years old.  There could be babies without men over sixty-five.  There couldn't be babies without other babies who had lived an hour or less after birth. 
>
> It was gibberish to Billy. (Vonnegut, 2005)

Our general tendency is to ascribe the having of good ideas to individuals. But the data collected in this experiment suggests that, like Billy in his fictional universe, we may be missing key elements of the story. The conception of ideas in collectively creative experiences might require different kinds of people, playing different kinds of roles, at different points in time. 

Going forwards in time from the birth of our idea, we can see that the person who says the idea out loud, Joe, is an important contributor. But so is everyone who "coo's" in affirmation even before he's finished saying it, and so is Helene who leaves shortly after to find the parts necessary to prototype it. After Helene and Neema build the prototype, Neema articulates her thinking behind it with an African proverb, which shapes the reflective conversation around it and sticks in the mind of the author, who brings the idea to it's latest concrete manifestation, the page (or the screen) you are reading.

Moving backwards in time from the moment of birth back into the gestation phase, we see Liz, Amy, and anna contributing to the playful, reflective environment and "riffing" on ideas. We can see Peter the catalyst bringing (and, crucially, generalizing) the idea of togetherness, and showing the video of the two machines grafted together. And several meters away there are Karsten and Julie, the curling parents who refused to give up on their handicapped drawing machine, and chose instead to graft it on to its helper.

They all have something to do with the birth of this idea. From this we can see that our initial description of the event, "Joe had an idea," has about equal grounds for plausibility as does an immaculate conception. [^onan] Yet when we look out into the world, we are surrounded by specters of the Genius Joe. Plenty of people know Watson and Crick discovered DNA, but they probably don't know Rosalind Franklin, the X-ray crystallographer whose work made their discovery possible, or any of the other members of the scientific "Scenius" that surrounded and informed their work (Maddox, 2003). As more and more scholars begin to look critically at colonial narratives about innovation and creativity, we begin to see that the cracks running through their foundations may extend all the way down to the idea of individual genius itself (Fischer & Vassen, 2011, Weisberg 1993 and Arendt 2013 in Sosa, 2019).

But one thing that Joe, Watson and Crick, Elon Musk, Einstein and the rest of the geniuses have going for them is the simplicity of their narrative. It would be tiresome to have to recite a list of 47 names every time we speak about the discovery of DNA. If Vonnegut's Billy can't handle the relatively straight-forward seven sexes it takes to conceive an Earthling, how will we ever understand and describe what it really takes to conceive a good idea? One approach is to try to reconceptualize a "good idea" as just the localized, temporal manifestation of a larger process, one that spans across more people and more time than we are used to thinking about. More (practice-based) research is needed.

[^joehadbaby]: If Joe were a woman we might say "Joe had a baby," and perhaps this would correspond a bit better to Joe's having had an idea, since it clearly came out of him. But the implied temporality of words for posession is misleading at larger timescales. Joe's ancestors and Joe's species and Joe's ecology had plenty to do with Joe's idea (and babies) in all but the most immediate time scales.
[^corpus]: This method could be used to create a shared corpus of data that could be used by different researchers to make different interpretations and conclusions. It would also be possible to share the entire dataset as well as a "zoomed in" view tailored to the idea under study.
[^hiding]:Too often research makes itself inaccessible to criticism from practitioners by placing questionable conclusions and relationships on a foundation of abstruse mathematical formulae. A little digging will often expose a formula's weakness or irrelevance to the question under study. But because the math looks impressively complicated, and because of the glorified epistemic position of mathematics and "the numbers" in our culture, many practitioners will assume that it doesn't make sense to them because they can't understand the math, instead of recognizing that it just doesn't make sense, full stop.
[^onan]: Following our metaphor to its logical conclusion, a person having an idea by themselves is perhaps similar to a person having sex by themselves. It's all well and good, but not something to be particularly proud of.
[^curling]: Curling is a game played on ice in which players frantically sweep in front of the path of a moving puck in order to smooth its way and influence its direction.

## References

Bateson, P. P. G., & Martin, P. (2013). Play, playfulness, creativity and innovation. Cambridge University Press.

Bevan, B., Gutwill, J. P., Petrich, M., & Wilkinson, K. (2015). Learning Through STEM-Rich Tinkering: Findings From a Jointly Negotiated Research Project Taken Up in Practice. Science Education, 99(1), 98–120. https://doi.org/10.1002/sce.21151

Bevan, B., Ryoo, J. J., Vanderwerff, A., Petrich, M., & Wilkinson, K. (2018). Making Deeper Learners: A Tinkering Learning Dimensions Framework. *Connected Science Learning*, *1*(7). https://www.nsta.org/connected-science-learning/connected-science-learning-july-september-2018-0/making-deeper-learners

Catmull, E. (2008). How Pixar Fosters Collective Creativity. (Cover story). Harvard Business Review, 86(9), 64–72. Business Source Complete.

Coles, A., & Sinclair, N. (2019). Re-thinking ‘concrete to abstract’: Towards the use of symbolically structured environments. *Canadian Journal of Science Mathematics and Technology Education*, *19*(6), 1–16. https://doi.org/10.1007/s42330-019-00068-4

Dewey, J. (2015). *Experience and education* (First free press edition 2015). Free Press.

Duckworth, E. (1972). The Having of Wonderful Ideas. *Harvard Educational Review*, *42*(2), 217–231. https://doi.org/10.17763/haer.42.2.g71724846u525up3

Fischer, G., & Vassen, F. (2011). Collective Creativity: Collaborative Work in the Sciences, Literature and the Arts. BRILL. [https://doi.org/10.1163/9789042032743](https://doi.org/10.1163/9789042032743)

Frere-Jones, A. (2014). Ambient Genius: The Working Life of Brian Eno. The New Yorker, 90 (82).

Giudici, C., Barchi, P., & Harvard Project Zero (Eds.). (2008). *Making learning visible: Children as individual and group learners ; RE PZ* (4. printing). Reggio Children.

Hippel, E. von. (2005). *Democratizing innovation*. MIT Press.

Kafai, Y. B. (2005). Constructionism. In R. K. Sawyer (Ed.), *The Cambridge Handbook of the Learning Sciences* (pp. 35–46). Cambridge University Press. https://doi.org/10.1017/CBO9780511816833.004

Kauffman, S. A. (2014). Prolegomenon to patterns in evolution. Biosystems, 123, 3–8. https://doi.org/10.1016/j.biosystems.2014.03.004

Krechevsky, M., Mardell, B., Rivard, M., & Wilson, D. G. (2013). *Visible learners: Promoting Reggio-inspired approaches in all schools*. Jossey-Bass.

Latour, B. (1999). Pandora’s hope: Essays on the reality of science studies. Harvard University Press.

Lévi-Strauss, C. (2000). The Savage Mind. Univ. of Chicago Press.

Maddox, B. (2003). The double helix and the ‘wronged heroine’. *Nature*, *421*(6921), 407–408. [https://doi.org/10.1038/nature01399](https://doi.org/10.1038/nature01399)

Papert, S. (1982). Mindstorms: Children, computers, and powerful ideas (Reprint). Harvester Press. 

Parjanen, S. (2012). Experiencing Creativity in the Organization: From Individual Creativity to Collective Creativity. Interdisciplinary Journal of Information, Knowledge, and Management, 7, 109–128. https://doi.org/10.28945/1580

Resnick, M., & Rusk, N. (1996). The Computer Clubhouse: Helping Youth Develop Fluency with New Media. *Proceedings of the 1996 International Conference on Learning Sciences*, 285–291.

Resnick, M. (2017). *Lifelong kindergarten cultivating creativity through projects, passion, peers, and play*. https://doi.org/10.7551/mitpress/11017.001.0001

Resnick, M., & Rosenbaum, E. (2013). Designing for Tinkerability. In M. Honey & D. Kanter (Eds.), *Design, make, play: Growing the next generation of STEM innovators*. Routledge.

Rinaldi, C. (2006). In dialogue with Reggio Emilia: Listening, researching and learning.

Sawyer, R. K. (2014). Group Creativity Music, Theater, Collaboration. Psychology Press.

Sawyer, R. K., & DeZutter, S. (2009). Distributed creativity: How collective creations emerge from collaboration. *Psychology of Aesthetics, Creativity, and the Arts*, *3*(2), 81–92. https://doi.org/10.1037/a0013282

Sosa, R. (2019). Accretion theory of ideation: Evaluation regimes for ideation stages. *Design Science*, *5*, e23. [https://doi.org/10.1017/dsj.2019.22](https://doi.org/10.1017/dsj.2019.22)

Turkle, S., & Papert, S. (1991). Epistemological Pluralism and the Revaluation of the Concrete. In S. Papert & I. Harel (Eds.), *Constructionism*. Ablex Publishing Corporation.

Vonnegut, K. (2005). *Slaughterhouse-five, or, The children’s crusade: A duty-dance with death* (Dial Press trade pbk. ed). Dial Press.

Vossoughi, S., & Bevan, B. (2014). Making and Tinkering: A Review of the Literature. National Research Council Committee on Out of School Time STEM, 57.

Wilkinson, K., & Petrich, M. (2013). *The art of tinkering: Meet 150+ makers working at the intersection of art, science & technology*. Weldon Owen.



