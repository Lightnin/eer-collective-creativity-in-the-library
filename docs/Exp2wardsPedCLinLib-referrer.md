# Article: Experiments towards a Pedagogy of Creativity and Learning in the Library

A revised version of the original article submitted with my PhD has been published by the Journal of Creative Library Practice, August 18, 2023, and can be found at the link below.

[https://creativelibrarypractice.org/2023/08/18/experiments-towards-a-pedagogy-of-creativity-and-learning-in-the-library/](https://creativelibrarypractice.org/2023/08/18/experiments-towards-a-pedagogy-of-creativity-and-learning-in-the-library/)

![JCLP Paper.](assets/JCLP-Paper-view.png)
