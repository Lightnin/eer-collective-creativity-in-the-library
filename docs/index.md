# Experimenting, Experiencing, Reflecting: Collective Creativity in the Library

by Amos Blanton

If you prefer, you may download and read a [PDF version](https://gitlab.com/Lightnin/eer-collective-creativity-in-the-library/-/raw/master/pandoc-thesis-template/EER-CCiL-AB_PhD_r1.01.pdf) of the dissertation.

This practice-based dissertation examines collective creativity from the perspective of a non-formal educator doing design-based research in the library. Building on theoretical foundations in Constructionism, the Reggio Emilia Approach, and Stuart Kauffman's theory of the adjacent possible, it describes new methods for creating conditions for the study of collective creativity in short-term, playful, open-ended, non-formal learning environments. Out of this process (and in collaboration with others) emerged *Playing with the Sun*, an open-ended construction kit and collection of tinkering activities designed to enable learners to build an intuitive sense of how different forms of sustainable energy work.

