

# Article: Recursive Prompting: A Method for Collectively Exploring a Design Space

Amos Blanton, MA EdS and Peter Dalsgaard, PhD.
Draft 8 June 2023.
Under review at *LearnX Design 2023: Futures of Design Education* conference.

## Abstract

Describes a new design method called Recursive Prompting in which participants collectively explore the possibilities of a design space in non-formal learning environments. Based on a pedagogical foundation in Tinkering and inspired by Kauffman’s adjacent possible, Recursive Prompting feeds forward ideas from past learners so that future learners can build on them. The goals are 1) to invite participants to an authentic experience of open-ended design as a context for developing their design skills and 2) to use this process to develop the method as a strong concept in design. A case study describing the current status is described as well as future directions for improvement of the method.

Keywords: recursive prompting; Strong Concept; Tinkering; Collaborative Design; non-formal education 

---

## Introduction

This paper describes a new design method called *Recursive Prompting* in which participants engage in a playful, collective design process in non-formal learning environments like libraries and science museums. Recursive prompting was inspired by a theory of evolution and innovation called the adjacent possible developed by the biologist and complexity theorist Stuart Kauffman (2014), and on the educational theory and practice of constructionism (Papert 1982), specifically as it pertains to Tinkering (Bevan, Petrich, and Wilkinson 2014). It is informed by years of practice designing and facilitating collective design activities, both in formal and nonformal learning environments. Recursive Prompting is designed to scaffold collective creativity, which for the purposes of this paper is defined as the emergence of innovative ideas from a group of individuals working and communicating together towards a shared purpose, similar to what Von Hippel observed in *innovation communities* (2005) and what Sawyer and DeZutter called distributed creativity (2009). In this paper we describe the pedagogical and scientific theories behind the method, its core elements, and position it as a strong concept in design as described by Höök and Löwgren (2012). It includes a case study in which recursive prompting was applied as a design method and evaluated against three criteria: progressive growth in complexity, clustering, and novel applications, and concludes with ways forward.

In recursive prompting activities, participants (adults or children age 10+) are prompted to create new prototypes inspired by documentation of prototypes made by past participants. These are in turn documented and fed forward to successive generations of participants as inspiration for their prototypes – hence the use of the term *recursive*. This distributed process broadens the range of individuals involved in the prototyping process and scaffolds the exploration of multiple generations of ideas. It is intended to demonstrate value both as a learning experience for the participants and a method for exploring a design space.

The pedagogical goals of the method are: 1) to invite learners into an authentic design experience to give them an opportunity for hands-on learning (Bevan, Gutwill, Petrich, and Wilkinson, 2015) , and 2) to enable them to experiment with and experience what it is like to be part of a collective exploration of a design space that spans both time and participants. In addition to their functional purpose, documentation of the prototypes is intended to stimulate learner reflection and awareness of the exploratory, open-ended, and iterative nature of the process of collective design. 

The conclusion suggests ways forward to improve the method.

## Background

### Constructionism and Tinkering

Constructionism (Papert and Harel, 1991) is an educational theory that emphasizes the importance of actively creating knowledge with and through artifacts. Constructionism was developed by Seymour Papert on the basis of the constructivist theory of Jean Piaget (Piaget 1973), which asserts that people create their own understanding of the world through experiences and interactions with it. By extension, the constructionist perspective suggests that it is possible to design circumstances for learners to develop their understanding of the world through hands-on, project-based activities in which they use their creativity to design, build, and create something tangible. As open-ended learning experiences they are designed to be supportive of epistemological pluralism (Turkle & Papert, 1990). The aim is for learners to gain a deeper, systemic understanding of the subject matter that can be applied across different domains. Because constructionism has always argued for the importance of both epistemological pluralism and learner agency, we see it as a good pedagogical foundation for efforts to develop pluriversal design education (Noel, 2020).

Recursive prompting has its practical foundations in the pedagogy of Tinkering. A comparatively recent outgrowth of constructionism developed at the Exploratorium Science Museum in San Francisco, tinkering refers to the act of playfully engaging with and learning about phenomena through exploratory hands-on project-based creative design processes (Bevan, B., Gutwill, J. P., Petrich, M., and Wilkinson, K et al. 2015). The pedagogy of Tinkering is an approach to designing and facilitating learning experiences that invite interest-driven learning using open-ended but carefully constrained provocations (Vossoughi and Bevan 2014). It is regularly put into practice in science museums, libraries, and makerspaces around the world.

The argument for the pedagogical value of Tinkering rests on the idea that doing what is essentially design in a tinkering activity is a means of developing important design skills (Martinez & Stager, 2019). The success of a Tinkering workshop can be evaluated by observation and documentation of the learners engaging with the experience. The Tinkering Studio at the Exploratorium developed a framework for evaluation called the Learning Dimensions of Making and Tinkering (Gutwill et al., 2015), which describes five learning dimensions and associated indicators, which has since been updated and made publicly available to Tinkering educators (Bevan, Ryoo, Vanderwerff, Petrich & Wilkinson 2018). Research suggests that the presence of these indicators is associated with the development of design skills and STEM learning (Bevan et al., 2015).

### The adjacent possible

> The adjacent possible consists of all those things (depending on the context, these could be ideas, molecules, genomes, technological products, etc.) that are one step away from what actually exists, and hence can arise from incremental modifications and recombinations of existing material (Tria et al. 2015). 

The adjacent possible is a theory for understanding the exploration of a space of possibilities which the biologist and complexity theorist Stuart Kauffman first proposed as an explanation for speciation in the fossil record. Simply put, the adjacent possible is what’s next door to whatever state something is in now. Before the Post-It note existed, it was an adjacent possible of the plain paper note taped to a wall. Once invented, the Post-It note became an “actual” from which new adjacent possibles could emerge, from making fish scales in kindergarten craft activities to tools for organizing and reorganizing collections of thoughts in design meetings.

Each time an adjacent possible transitions into an “actual,” it changes the space of possibilities not only for itself but also for the entire system of which it is a part. As a result, each movement into an adjacent possible is not only a potential optimization within the current context, but also has the potential to be the introduction of a new evolutionary niche from which new adjacent possibles can emerge. Kauffman (2014) provides an example from the evolution of the swim bladder, which allows fish to maintain neutral buoyancy at different heights within the water column. Believed to have evolved from the primitive lungs of a lungfish, the swim bladder made possible a new ecological niche in the oceans which thousands of species soon evolved to fit into.

Design can be framed as an exploration of the adjacent possible. If one were to design a pedagogy for teaching people to navigate the adjacent possible, it would likely share many qualities with the pedagogy of Tinkering. Tinkering activities are open-ended within a carefully designed set of materials and constraints. The frame is set (i.e. build a drawing machine), but what will be created is unknown at the start and dependent on the learner’s curiosity, interests, and knowledge. Facilitation strategies are designed to help learners engage with the process of tinkering, which Resnick & Rosenbaum (2013) describe this way:

> Instead of the top-down approach of traditional planning, tinkerers use a bottom-up approach. They begin by messing around with materials (e.g., snapping Lego bricks together in different patterns), and a goal emerges from their playful explorations (e.g., deciding to build a fantasy castle). Other times, tinkerers have a general goal, but they are not quite sure how to get there. They might start with a tentative plan, but they continually adapt and renegotiate their plans based on their interactions with the materials and people they are working with.

Like evolution, tinkering proceeds step-by-step in the direction of greater complexity, but without a pre-defined end state or plan in mind (Jacob 1977). 

### Recursive Prompting as a Strong Concept

In their influential 2012 paper, Höök and Löwgren propose the notion of "strong concepts" as a type of intermediate-level knowledge that is generative for design and can be applied in a range of design situations (Höök and Löwgren 2012). Strong concepts reside at a level above singular cases such that they are applicable in multiple cases and potentially across domains, are concerned with interactive behavior, can be embedded into or inform the design of artifacts, and reflect the practice and use of artifacts. In a similar vein, Dalsgaard and Dindler (2014) propose the notion of bridging concepts as a solution to the challenge of connecting theory and practice. Bridging concepts are defined three constituent components: a theoretical basis, a set of design articulations for how the concept may be expressed, and a range of examples that illustrate their potential use. Whereas conceptual constructs are primarily informed by and seeks to further contribute to the development of theory, bridging concepts build on and seek to further develop both theory and practice.

While the concept of recursive prompting can be said to embody characteristics from both of the above mentioned constructs, we consider it primarily as a strong concept under development, with a particular emphasis on the generative potentials it holds for informing collaborative development and exploration of design spaces. While most prior examples of strong concepts pertain to the design of interactive artifacts, recursive prompting stands out in that it concerns a way of orchestrating a collective design process.

## Methodology

In order to put recursive prompting to the test in practice, we ran a recursive prompting case study, which we report on in this paper. The research question was: *Can the method of recursive prompting enable unspecified participants to contribute to an open-ended exploration of a design space that results in progressive growth in complexity, clustering around the emergence of valuable ideas, and novel applications?*

 We documented this session via video recordings (Penn-Edwards 2004), field notes (Patton 2005), and unstructured interviews (Kvale 1994) in response to the emergent actions of study participants. Moreover, the development of all prototypes in the experiment were documented on a physical recursive prompting map (see figure 1). Participants were recruited by setting up the recursive prompting map and worktable with materials in the social / coffee area of a medium sized academic conference in [University redacted] called [conference name redacted] with approx. 200 attendees. Any conference attendee who approached and showed interest was invited to participate. No specific data on attendees was collected aside from release forms for to enable us to collect field notes and make recordings. The approximate age range of participants was estimated to be 20-65 with roughly equal gender participation. There was a wide range of technical experience, with some showing familiarity with the electronic elements and others for whom it was likely their first encounter with these technologies.

Upon completion of the experiment, we carried out an analysis of the collected data, including video recordings, field notes, and the structure of influences documented on the recursive prompting map. In our analysis, we examined the data for evidence of three phenomena which we consider indicators of successful recursive prompting: progressive growth in complexity, clustering, and novel applications. We describe these indicators in more detail in the findings section.

## Method of Recursive Prompting

The pedagogical value of Tinkering as an individualized learning experience aimed at citizens in non-formal learning environments has been established (Bevan et al., 2015). The goal is to build on that foundation to develop a method to invite learners into a collective design experience. Such a method could make clear how collective design processes benefit from diversity in terms of knowledge and perspective of participants (Hoever, van Knippenberg, van Ginkel, & Barkema 2010; Bercovitz & Feldman 2011). We believe it is important that such a method should demonstrate design utility as well as educational value. At this stage we are utilizing practice based research in non-formal learning environments to offer 1) an engaging design learning experience to participants, while 2) attempting to develop the method itself. 

As Schön describes in the Reflective Practitioner (Schön 1983), sketching is an iterative process in which the sketcher receives feedback from their own sketch, which then informs the subsequent ideas that emerge. They are engaging in what Schön described as a "conversation with the material." Recursive prompting is a strategy of using documentation to extend that conversation to more people, and with it create a map of adjacent possibles in the design space that should prove useful in subsequent design work. The map of documentation serves as a kind of public sketchbook, showing the different design ideas and emergent domains explored by the collective.

Once the recursive prompting activity is complete, the documentation can be analyzed and archived for future analysis.  It can also be used as the starting prompt for future learning activities. 

### Participant Experience Flow in Recursive Prompting Activities

#### Introductory Phase

The introductory phase begins when participants first encounter the activity in an informal space. Often the first thing they see is the ongoing map of documentation of the recursive prompting process. When approaching the map of documentation, the viewer should be able to quickly understand where the different regions of the design space lie, and where the present moment in time is represented. This constitutes the frontier of the design process. The eye can then track backwards from the present moment in what amounts to a (short term) historical tour of what came before and the relationships between the various prototypes across time.  

Facilitators (approximately 2 for every 10 participants working simultaneously) greeting new potential participants should give a brief tour of one evolutionary line of prototypes to explain a few of the ideas already developed by past participants. If they choose to participate, this is followed by an introduction to the tools and building materials, and stating the prompt. Tools and materials for prototyping must be easy for participants to understand and to use, along the lines of what Papert and Resnick referred to as a “low floor” (Resnick 2017).

#### Build Phase

Participants build creative prototypes for as long as they wish to in a collegial, lightly facilitated, and low-stakes social environment in which they can see, reflect on, and discuss what one another are doing. They should feel welcome to borrow and repurpose any ideas from the map or from one another, and to ask for help anytime. 

During the build phase the map of documentation of previous prototypes serves as a source or information about past ideas and iterations one might learn from or copy, as well as showing what has not yet been explored within the space of possibilities. It should be visible and close by. 

#### Outro Phase

Once a prototype is completed, it is documented and added to the map of documentation in such a way that future participants can easily understand its qualities and evaluate it as a potential starting point for their own explorations. It should be recorded and presented in such a way that makes it easy for a viewer to borrow or repurpose ideas from it to use for their own creative exploration. Information gathered should help the facilitator see where to place the project on the map in relation to other projects, so as to make those relationships clear. 

![(Article Figure 1) The live recursive prompting map at the end of a daylong workshop. iPads shown on the right were playing video loops of prototypes. An Interactive digital representation is available at: https://app.milanote.com/1Pq3fa1EpTdz6L?p=d8NEXrOl73g](assets/map-physical-RP.jpg)

## Case Study: Recursive Prompting at the CES Conference

The case study of a recursive prompting activity described here was run at the Cultural Evolution Society conference at Aarhus University in 2022, by two facilitators, one of them an author of this paper. A digital version of the recursive prompting map, including all of the video data collected of the prototypes described below, can be viewed on Milanote at this URL: ( [https://app.milanote.com/1Pq3fa1EpTdz6L?p=d8NEXrOl73g](https://app.milanote.com/1Pq3fa1EpTdz6L?p=d8NEXrOl73g) )

Prior to the opening of the conference, we made a large cardboard map titled *Evolution of Solar Art*, with time marks on the horizontal axis, and mounted it in the coffee area of the conference where we knew many passersby would see it (Figure 1). Next to this we placed a work table large enough for 6 people to work at a time with prototyping materials from the *Playing with the Sun* construction kit ([https://resources.playingwiththesun.org/Const-Kit-Overview/](https://resources.playingwiththesun.org/Const-Kit-Overview/)), developed by one of the authors as part of research developing tinkering activities in collaboration with Aarhus Public Libraries. Materials included 1 watt solar panels, solar-engines that store solar energy until there is enough to make a pulse that will drive a motor, patch cables, gear motors with attached 3D printed hubs, markers, binder clips to act as pen holders, and small mirrors. Short sections of bendable plumber’s strap and removable push rivets were provided as an open-ended framework for building structures. We made a simple example project and made a brief video of it with the first of 8 iPads we had in reserve, and then placed the iPad playing the video in a loop in the left most position of the recursive prompting board.

![(Article Figure 2) Participants at the worktable at the start of prototyping session.](assets/worktable-RP.jpg)

Throughout the day, participants approached the board or the worktable out of curiosity (Figure 2). A facilitator explained that we were doing research about how ideas build on one another in open-ended activities, answered any questions, and invited them to participate (Figure 3). Those that chose to join were asked to sign a release to permit filming and use of gathered data for research. We introduced them to the building materials and demonstrated how to make basic connections to make the motor turn. Then we invited them to build something interesting that draws using solar power.

There were 20 participants across the day between the opening hours of 10 to 17:00, with busier times clustered around conference mealtimes and coffee breaks, and slower times around keynotes. Most participants worked alone with a few working in pairs. Many more people viewed the activity than could participate due to limits on space. 

![(Article Figure 3) A facilitator demonstrates how the construction materials work for a new participant.](assets/facilitate-RP.jpg)

When each participant or pair of participants finished (ranging between approx. 10 to 50 mins), we recorded a brief video of their creation in which we asked them 1) to name it, 2) to say a little about it, and 3) asked them if they were inspired by anything else they saw. We then placed the iPad on the map at the corresponding time on the X axis with the video playing on a constant loop. (The Y axis position was not indicative.) We wrote a brief description of the prototype next to the iPad. If they indicated that a previous prototype on the map inspired theirs, we placed a line of red tape between the two prototypes. Once all iPads were up on the map we took the left-most on the X axis (which had been on the map for the longest time) and replaced it with a sketch before using it to record and display the next prototype. At the conclusion we uploaded the videos to a “virtual” version of the map in software called Milanote (Figure 4).

![(Article Figure 4) The completed milanote board with video data for each prototype, visible at: https://app.milanote.com/1Pq3fa1EpTdz6L?p=d8NEXrOl73g](assets/recprompt-3-rp.png)

### Findings from the case

We examined the data from video recordings, field notes, and unstructured interviews for evidence of three phenomena that we consider to be indicators of a successful recursive prompting activity: 

#### Progressive Growth in Complexity

Progressive growth in complexity suggests that there has been an accretion of valuable ideas over time in the prototypes collected, with later ones showing a greater degree of complexity or sophistication (in any domain) than earlier ones. This would be an indication that the method of recursive prompting supports or encourages the feeding forward of valuable ideas from past prototypes such that they are worth integrating in future ones, forming a foundation for progressive explorations of the adjacent possible in the design space. 

There is little evidence of progressive growth in complexity in the case study data. Such evidence would take the form of one or more ideas appearing in one prototype that form the foundation for further exploration of adjacent possibles, appearing in at least 2 generations of subsequent prototypes. Of the 17 video recordings, three contain references by participants to inspiration coming from others working alongside them at the worktable, something which is common in tinkering activities. But this inspiration does not appear to span multiple generations of prototypes.

#### Clustering

Clustering is indicated by the emergence of prototype(s) that serve as a common ancestor from which many subsequent projects took inspiration, and are therefore linked back to it in the map of documentation. Clustering is caused by many participants finding value in some quality of the prototype at the center of the cluster. This may come from perceived value in any domain, from aesthetic, to engineering, to anything else. A project around which clustering has occurred serves as a common ancestor of future designs, as well as indicating a domain that participants value.

Our analysis revealed no clear evidence of clustering in this case study. On the map of documentation, only a single prototype is marked as inspiration to two subsequent prototypes (excepting the starting “base model,” which is excluded from our analysis). Judging from impressions of facilitators during the event and statements made by a few of the participants, the most plausible explanation for why there was no clustering is that most of the participants were primarily interested in exploring their own ideas of what they could make with the construction kit. 

#### Novel Applications

One of the criteria for success used to evaluate the design of tinkering activities is referred to by tinkering educators as *solution diversity*, meaning that one sees a wide variety of different projects that come from the activity. Such variety indicates that different participants have been able to integrate their own ideas and interests into the process, shaping the results in varying ways, enabling each of them to experience what Eleanor Duckworth referred to as “the having of wonderful ideas” (1972) . In addition to the argument that this makes the participant’s experience pedagogically valuable, it also suggests that the activity design has enabled the expression of the diversity that exists in the group. 

Novel prototypes demonstrate a unique or unexpected purpose, application, or aesthetic. In order to meet our success criteria, novel prototypes should also suggest a new domain of design exploration. Three prototypes from our case study met this criteria. 

The fourth participant of the day created a simple rotating sculpture on a base instead of a drawing machine. Two facilitators found this interesting and began playing with a simple motor and solar panel combination on a fixed base. This prototype rotated when oriented towards the sun and stopped once it rotated out of the sunlight. They could then reflect additional light onto its solar panel with mirrors. This became a two person cooperative game in which each player reflected light onto the panel when it rotated towards them (see *Spin Game* in the Milanote map data linked above). 

Subsequently, a co-facilitator [name removed] made multiple versions of this prototype, attached mirrors to the back of the panels, and then placed them in relation to one another so that each machine would at times reflect light onto another machine (see *Panels / Mirrors* on the Milanote Map). By breaking the frame of the prompt, the rotating sculpture made possible the subsequent sculptural builds, which then explored the novel idea of each prototype giving feedback to others in the form of reflected light. 

The *fur Maker* is another novel prototype that meets the criteria described above. It uses the motion of the wheel to draw repeated parallel lines that look like animal fur. As it is hand held, it can be used to draw these lines anywhere on the paper. It suggests the possibility of hand-held drawing devices that convert solar energy into a means of agitating the pen and creating different patterns, a new aesthetic and practical application. 

The third novel prototype emerged early on when a participant made a prototype called *Bi-directional motor* system that demonstrated a means of orienting two solar panels in such a way that the machine maintains its power through most of its degrees of rotation with respect to the sun. Normally solar drawing machines lose power the more they rotate their panel away from perpendicular to the sun’s rays. The builder of *bi-directional motor system* noticed this and chose to develop an engineering strategy for working around this inherent limitation of the materials.

These three prototypes define three distinct domains for further exploration in the design of the construction kit and activities: 1. Interactive games (the *rotating sculpture*) 2. Aesthetics (the *fur maker*), and 3. Engineering (the *bi-directional motor system.)* Moreover, this speaks to the third criteria for strong concepts set forth by Höök and Löwgren (2012), namely *substanticity*, the concept's relevance and potential for use in designing new instances.

## Discussion

François Jacob argued that the process of evolution was more like tinkering than engineering (1977). Kauffman’s adjacent possible (2014) builds on that idea by providing a descriptive model of how that process works. Recursive prompting is an attempt to develop a method for applying these ideas in the context of design and design education by creating a context for playful tinkering, reflective selection of where to invest energy in exploring adjacent possibles, and the shaping of emergent ideas into prompts for successive generations of participants. 

Because it is a drop-in activity designed to welcome diverse, intrinsically motivated participants in a non-formal learning space, we see recursive prompting as potentially useful to efforts to develop pluriversality in design education (Noel, 2020). By highlighting the diversity of prototypes and perspectives from different participants, we hope to show the value of a broad range of epistemic perspectives (Turkle & Papert, 1991) and how these can lead to new and valuable domains of design exploration through methods that value collective creativity. There is already evidence that diversity in the composition of teams increases the chances of innovation (Johansson, 2004; Paulus, 2000 in Parjanen 2012), perhaps methods like this one can one day be used to explore that question further.

During the activities, participants demonstrated a range of indicators of learning, described under each of the Tinkering Studio’s *Learning Dimensions of Making and Tinkering* (Bevan et. al. 2018), especially those associated with the Learning Dimensions *Initiative and intentionality* and *Creativity and Self-expression*. We can infer the likelihood of pedagogical value in this learning experience from the presence of these indicators, and the established literature on the pedagogical value of Tinkering (Bevan et. al. 2015). We argue that using Tinkering as a foundation for developing new design methods is likely to confer baseline value as a design learning experience in addition to what the method itself offers.

In addition to the pedagogical value offered by an engaging and playful Tinkering experience, we believe recursive prompting has the potential to become a useful method for designers to involve citizens in non-formal learning environments like libraries and museums in the exploration of a design space. It opens up the possibility of inviting a large, diverse group of participants to drop-in and engage with a design process without requiring specialized skills. The documentation collected and displayed on the recursive prompting map is likely to show not only novel concepts and emergent domains, but also to provide evidence of which domains a population of participants is interested in. 

## Conclusion and future work

The recursive prompting case study described herein proved to be highly generative and led to prototypes that define interesting new domains. The *fur maker*, for example, could easily be used as a sample project for the prompt “Build a tool you hold in your hand that helps to draw interesting designs.” Similarly, one could imagine inviting successive generations of participants to explore the engineering problem of how to provide constant power irrespective of their machine’s angle to the sun (as shown by the prototype *Bi-directional motor system).* Neither of these possibilities were identified in the design space prior to this recursive prompting session. Each novel prototype suggests a different domain with potential for further exploration - an adjacent possible. It should be possible to run the activity again using each of these novel prototypes as a starting point, inviting future participants to prototype further within the design domains they describe. Perhaps the same could be done with the output from this next workshop.  

The recursive prompting case study did not show evidence of two out of three success indicators: clustering and progressive growth. In future experiments we will make changes to the method to try to design towards these goals. We may try to highlight emergent sub-prompts as potential starting points for future participants by writing them clearly on the map. Should something like a *fur-maker* emerge, we might write a specific sub-prompt inspired by it on the recursive prompting map next to it, i.e.: “Make a handheld solar-powered drawing device.” We would do this in order to more directly position emergent ideas as potential sub-prompts for new participants. This may help to encourage both clustering and greater progressive growth in complexity over time. 

In subsequent iterations we will try giving new participants a longer orientation period - perhaps 3-5 minutes - in which they are given an opportunity to get used to the building materials before we give the prompt. Alternatively, we may try offering them a closed-ended task to complete first, such as assemble a pre-determined structure or base model to work from. Afterwards we can invite them to leave the worktable and take a closer look at the recursive prompting board to select a prototype or sub-prompt from which to take inspiration. More familiarity with the building materials and how they work should enable them to better understand the information presented on the board, and its relevance to the shared exploration. 

## References

Bercovitz, J., & Feldman, M. (2011). The mechanisms of collaboration in inventive teams: Composition, social networks, and geography. *Research Policy, 40*, 81-93.

Bevan, B., Gutwill, J. P., Petrich, M., and Wilkinson, K. (2015). Learning Through STEM-Rich Tinkering: Findings From a Jointly Negotiated Research Project Taken Up in Practice. Science Education, 99(1):98–120. Publisher: John Wiley & Sons, Inc.

Bevan, B., Petrich, M., and Wilkinson, K. (2014). TINKERING Is Serious PLAY. Educational Leadership, 72(4):28–33. Publisher: Association for Supervision & Curriculum Development.

Bevan, B., Ryoo, J. J., Vanderwerff, A., Petrich, M., & Wilkinson, K. (2018). Making Deeper Learners: A Tinkering Learning Dimensions Framework. *Connected Science Learning*, *1*(7). https://www.nsta.org/connected-science-learning/connected-science-learning-july-september-2018-0/making-deeper-learners

Dalsgaard, P. and Dindler, C. (2014). Between theory and practice: bridging concepts in hci research. In Proceedings of the SIGCHI conference on Human Factors in Computing Systems, pages 1635–1644.

Duckworth, E. (1972). The Having of Wonderful Ideas. Harvard Educational Review, 42(2):217–231.

Gutwill, J. P., Hido, N., & Sindorf, L. (2015). Research to Practice: Observing Learning in Tinkering Activities. *Curator: The Museum Journal*, *58*(2), 151–168. https://doi.org/10.1111/cura.12105

Hoever, I.J., van Knippenberg, D., van Ginkel, W., & Barkema, H.G. (2010). Fostering team creativity: perspective taking as key to unlocking diversity's potential. *The Journal of applied psychology, 97 5*, 982-96 .

Höök, K. and Löwgren, J. (2012). Strong concepts: Intermediate-level knowledge in interaction design research. ACM Transactions on Computer-Human Interaction (TOCHI), 19(3):1–18.

Jacob, F. (1977). Evolution and Tinkering. Science, 196(4295):1161–1166.

Kauffman, S. A. (2014). Prolegomenon to patterns in evolution. Biosystems, 123:3–8.

Kvale, S. (1994). Interviews: An introduction to qualitative research interviewing. Sage Publications, Inc.

Martinez, S. L., & Stager, G. (2019). *Invent to Learn: Making, Tinkering, and Engineering in the Classroom*. Constructing Modern Knowledge Press. https://books.google.dk/books?id=gjjHwAEACAAJ

Noel, L.-A. (2020). Envisioning a pluriversal design education. *Pivot 2020: Designing a World of Many Centers*. DRS Pluriversal Design SIG Conference 2020. https://doi.org/10.21606/pluriversal.2020.021

Papert, S. (1982). Mindstorms: children, computers, and powerful ideas. Number 14 in Harvester studies in cognitive science. Harvester Press, Brighton, reprint edition.

Papert, S. and Harel, I. (1991). Situating constructionism. constructionism, 36(2):1–11.

Parjanen, S. (2012). Experiencing Creativity in the Organization: From Individual Creativity to Collective Creativity. Interdisciplinary Journal of Information, Knowledge, and Management, 7, 109–128. https://doi.org/10.28945/1580

Patton, M. Q. (2005). Qualitative research. Encyclopedia of statistics in behavioral science.

Penn-Edwards, S. (2004). Visual evidence in qualitative research: The role of videorecording. The Qualitative Report, 9(2):266–277.

Piaget, J. (1973). Psychology and epistemology.

Resnick, M. (2017). Lifelong kindergarten cultivating creativity through projects, passion, peers, and play. OCLC: 1227489657.

Resnick, M. and Rosenbaum, E. (2013). Designing for Tinkerability. In Honey, M. and Kanter, D., editors, Design, make, play: growing the next generation of STEM innovators. Routledge, New York, NY.

Sawyer, R. K. and DeZutter, S. (2009). Distributed creativity: How collective creations emerge from collaboration. Psychology of Aesthetics, Creativity, and the Arts, 3(2):81–92.

Schön, D. A. (1983). The reflective practitioner: how professionals think in action. Basic Books, New York.

The Tinkering Studio at the Exploratorium. (2023). *Learning Dimensions of Making and Tinkering: A professional development tool for educators | Exploratorium*. https://www.exploratorium.edu/tinkering/our-work/learning-dimensions-making-and-tinkering

Tria, F., Loreto, V., Servedio, V. D. P., and Strogatz, S. H. (2015). The dynamics of correlated novelties. Scientific Reports, 4(1):5890.

Turkle, S., & Papert, S. (1990). Epistemological Pluralism: Styles and Voices within the Computer Culture. *Signs: Journal of Women in Culture and Society*, *16*(1), 128–157. https://doi.org/10.1086/494648

Turkle, S., & Papert, S. (1991). Epistemological Pluralism and the Revaluation of the Concrete. In S. Papert & I. Harel (Eds.), *Constructionism*. Ablex Publishing Corporation.

Vossoughi, S. and Bevan, B. (2014). Making and Tinkering: A Review of the Literature. National Research Council Committee on Out of School Time STEM, page 57.