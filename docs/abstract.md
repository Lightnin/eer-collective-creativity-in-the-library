# Experimenting, Experiencing, Reflecting: Collective Creativity in the Library{.unnumbered}

This practice-based dissertation examines collective creativity from the perspective of a non-formal educator doing design-based research in the library. Building on theoretical foundations in Constructionism and Tinkering, the Reggio Emilia Approach, and Stuart Kauffman's theory of the adjacent possible, it describes new methods for creating conditions for the study of collective creativity in short-term, playful, open-ended, non-formal learning environments. Out of this process (and in collaboration with others) emerged *Playing with the Sun*, an open-ended construction kit and collection of tinkering activities designed to enable learners to build an intuitive sense of how different forms of sustainable energy work.





**Eksperimentere, opleve, reflektere: Kollektiv kreativitet I biblioteket**

Denne praksisbaserede afhandling undersøger kollektiv kreativitet set fra perspektivet af en uformel underviser, der laver designbaseretforskning på et bibliotek. Med udgangspunkt i et teoretiske grundlag inden for konstruktionisme og tinkering, Reggio Emilia pædagogikken og Stuart Kauffmans teori om det tilstødende mulige, beskriver afhandlingen nye metoder til at skabe betingelser for studiet af kollektiv kreativitet på kort sigt i legende, åbne, uformelle læringsmiljøer. Ud af denne proces (og i samarbejde med andre) opstod Playing with the Sun, et åbent byggesæt og en samling af tinkeringaktiviteter designet til at give eleverne mulighed for at opbygge en intuitiv fornemmelse af, hvordan forskellige former for bæredygtig energi fungerer.